This is the final project with Certification recieved from Coursera.

The project is a web application for a restaurant. The project covers all aspects of React, from front-end libraries like reactstrap to backend communication (API calls) with fetch.

To run the project on your own machine:

1. Copy the project to your machine by running this on the command line/terminal -> git clone https://smurphy7440@bitbucket.org/smurphy7440/restorante.git

2. Then cd into the project folder

3. Run -> npm install   # This will install all the third-party libraries

4. To start the server open a separate terminal go the json-server folder of the project (cd json-server) and run -> json-server --watch db.json -p 3001 -d 2000

5. In the root of the project folder: Run -> yarn start      OR     Run -> npm start






